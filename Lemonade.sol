pragma solidity ^0.4.24;

contract LemonadeStand {
    
    address owner;
    
    uint skuCount;
    
    enum State { ForSale, Sold, Shipped }
    
    struct Item {
        string name;
        uint sku;
        uint price;
        State state;
        address seller;
        address buyer;
    }
    
    // Map sku number ot item
    mapping(uint => Item) items;
    
    // Events
    event ForSale(uint skuCount);
    
    event Sold(uint sku);
    
    event Shipped(uint sku);
    
    // Modifiers
    modifier onlyOwner() {
        require(msg.sender == owner, 'Only owner');
        _;
    }
    
    // Define a modifier that verifies the caller
    modifier verifyCaller(address _address) {
        require(msg.sender == _address);
        _;
    }
    
    modifier paidEnough(uint _price) {
        require(msg.value >= _price, 'Not paid enough');
        _;
    }
    
    modifier forSale(uint _sku) {
        require(items[_sku].state == State.ForSale, 'Item must be for sale');
        _;
    }
    
    modifier sold(uint _sku) {
        require(items[_sku].state == State.Sold);
        _;
    }
    
    modifier checkValue(uint _sku) {
        _;
        uint price = items[_sku].price;
        uint amountToRefund = msg.value - price;
        msg.sender.transfer(amountToRefund);
    }
    
    constructor() public {
        owner = msg.sender;
        skuCount = 0;
    }
    
    function addItem(string _name, uint _price) onlyOwner public {
        // Increment Sku
        skuCount += 1;
        
        // Emit appropriate event
        emit ForSale(skuCount);
        
        // Add the new item to inventory and mark for sale
        items[skuCount] = Item({name: _name, sku: skuCount, price: _price, state: State.ForSale, seller: msg.sender, buyer: 0});
    }
    
    function buyItem(uint sku) forSale(sku) paidEnough(items[sku].price) checkValue(sku) public payable {
        address buyer = msg.sender;
        uint price = items[sku].price;
        
        // Update buyer
        items[sku].buyer = buyer;
        
        // Update state
        items[sku].state = State.Sold;
        
        // Transfer Money to seller
        items[sku].seller.transfer(price);
        
        // Refund any extra value sent over
        
        // Emit appropriate event
        emit Sold(sku);
    }
    
    function shipItem(uint _sku) sold(_sku) verifyCaller(items[_sku].seller) public {
        items[_sku].state = State.Shipped;
        emit Shipped(_sku);
    }
    
    function fetchItem(uint _sku) public view returns (string name, uint sku, uint price, string stateIs, address seller, address buyer, address psst) {
        uint state;
        name = items[_sku].name;
        sku = items[_sku].sku;
        price = items[_sku].price;
        state = uint(items[_sku].state);
        if (state == uint(State.ForSale)) stateIs = "For Sale";
        else if (state == uint(State.Sold)) stateIs = "Sold";
        else if (state == uint(State.Shipped)) stateIs = "Shipped";
        seller = items[_sku].seller;
        buyer = items[_sku].buyer;
        psst = owner;
    }
}
